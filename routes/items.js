var express = require('express');
var router = express.Router();

var names = require("ass-db/lib/names");
var items = require("ass-db/lib/items");

router.get('/getrandomname', function(req, res, next) {
    names.getRandomName(function(err,name){
        if(err)
            return next(err);
        res.json(name);
    });
});

router.get('/getfirstnamesstartingwith', function(req, res, next) {
  names.getFirstNamesStartingWith(req.query.start,function(err,names){
        if(err)
            return next(err);
        res.json(names);
    });
});

router.get('/getlastnamesstartingwith', function(req, res, next) {
  names.getLastNamesStartingWith(req.query.start,function(err,names){
        if(err)
            return next(err);
        res.json(names);
    });
});

router.get('/gettags', function(req, res, next) {
  items.getTags(function(err,names){
        if(err)
            return next(err);
        res.json(names);
    });
});

/* GET items listing. */
router.get('/', function(req, res, next) {
    return next(new Error("not implemented"));
});

/* GET item data. */
router.get('/:id', function(req, res, next) {
    return next(new Error("not implemented"));
});

/* ADD item */
router.post('/create', function(req, res, next) {
    return next(new Error("not implemented"));
    //res.json('respond with a resource');
});

/* EDIT item */
router.post('/edit/:id', function(req, res, next) {
    return next(new Error("not implemented"));
});

/* DELETE item */
router.delete('/:id', function(req, res, next) {
    return next(new Error("not implemented"));
});

module.exports = router;
