var express = require('express');
var router = express.Router();

var Iatas = require("ass-db/lib/iata");
var Containers = require("ass-db/lib/containers");

/* GET random iata */
router.get('/getrandomiata', function(req, res, next) {
    Iatas.getRandomNotUsed(1, function(err,items){
        if(err)
            return next(err);
        res.json(items);
    });
});

/* GET containers listing. */
router.get('/', function(req, res, next) {
    Containers.getContainers(function(err, containers){
        if(err)
            return next(err);
        res.json(containers);
    });
});

/* GET containers types listing. */
router.get('/types', function(req, res, next) {
    Containers.getContainerTypes(function(err, containertypes){
        if(err)
            return next(err);
        res.json(containertypes);
    });
});


/* GET container data. */
router.get('/:id', function(req, res, next) {
    return next(new Error("not implemented"));
});

/* ADD container */
router.post('/create', function(req, res, next) {

    console.dir(req.body);

    if(!req.body.id)
      return next(new Error("id not provided"));
    if(!req.body.iata)
      return next(new Error("iata not provided"));
    if(!req.body.containertype)
      return next(new Error("containertype not provided"));
    if(!req.body.title)
      return next(new Error("title not provided"));

    var requestedContainer = {
      id: req.body.id,
      iata: req.body.iata,
      containertype: req.body.containertype,
      title: req.body.title
    };

    Containers.create(requestedContainer, function(err, createdContainer) {
      if(err) return next(err);
      if(!createdContainer) return next(new Error('container not created'));

      return res.json(createdContainer);
    });

});

/* EDIT container */
router.post('/edit/:id', function(req, res, next) {
    return next(new Error("not implemented"));
});

/* DELETE container */
router.delete('/:id', function(req, res, next) {
    return next(new Error("not implemented"));
});

module.exports = router;
