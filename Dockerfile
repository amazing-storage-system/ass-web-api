FROM node:14-alpine

#ass-db
WORKDIR /ass/ass-db
COPY ./ass-db/package.json .
COPY ./ass-db/yarn.lock .
RUN yarn install
COPY ./ass-db .

#ass-web-api
WORKDIR /ass/ass-web-api
COPY ./ass-web-api/package.json .
COPY ./ass-web-api/yarn.lock .
RUN yarn install
COPY ./ass-web-api .

EXPOSE 8080

CMD [ "yarn", "start" ]
