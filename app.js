var createError = require('http-errors');
var express = require('express');
bodyParser = require("body-parser")

var cookieParser = require('cookie-parser');
var logger = require('morgan');

var containersRouter = require('./routes/containers');
var itemsRouter = require('./routes/items');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

app.use(function(req,res,next){
  res.setHeader("Access-Control-Allow-Origin", "*");
  next();
});

app.use('/api/items', itemsRouter);
app.use('/api/containers', containersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.error(err);
  // render the error page
  res.status(err.status || 500);
  res.json({status: err.status, message: err.message});
});


module.exports = app;
